from django.apps import AppConfig


class EximporterConfig(AppConfig):
    name = 'eximporter'
