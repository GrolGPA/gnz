from django.db import models
from repository.models import Deals
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.core.files import File
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.http import HttpResponse, HttpResponseRedirect




class Export(models.Model):

    template_file = 'template.txt'
    file_puth = 'file_storage/file.txt'

    def export(self, query):

        content = render_to_string(self.template_file, {'query_set': query})
        # Saving to the file
        file = default_storage.save(self.file_puth, ContentFile(content))

        file_url = default_storage.url(file)




class XmlExport(Export):
    template_name = '../templates/export_xml.html'
    # success_url = reverse_lazy('deals')

    # def __init__(self, arg = 'all'):
    #     url = self.export_xml()
    #     self.url = url
    #     # return 'file_storage/content.xml'

    # @staticmethod
    def export(self, *args, **kwargs):

        #Rendering template with data structure and return xml data
        xml = render_to_string('xml_template.xml', {'query_set': Deals.objects.all()})

        # Saving to the xml file
        default_storage.save('file_storage/content.xml', ContentFile(xml))

        # file_url = default_storage.url(file)

        # return render('export_file', 'file_storage/content.xml')
        # return reverse('export_file', args=[str('file_url')])
        return HttpResponse('Success!')