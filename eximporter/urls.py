from django.urls import include, path, re_path
# from django.urls import *
from . import views
from eximporter.views import ExportView, XmlExportView

urlpatterns = [
        # path('', TemplateView.as_view(template_name='../templates/export.html'), name='export'),
        re_path(r'', ExportView.as_view(), name='export'),
        re_path(r'^export/xml/$', XmlExportView.as_view(), name='export_xml'),
]