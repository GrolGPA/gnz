from django.shortcuts import render
from django.views import generic
from . models import XmlExport
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

"""
Classes of the export view
"""

class ExportView(LoginRequiredMixin, TemplateView):

    template_name = '../templates/export.html'

class XmlExportView(ExportView):

    xml = XmlExport()
    result = xml.export('month')









