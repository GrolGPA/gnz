from django.contrib import admin

# Register your models here.
from .models import Employees, Clients, Deals


admin.site.register(Employees)

# Register the Admin classes for Clients using the decorator

@admin.register(Clients)
class ClientsAdmin(admin.ModelAdmin):
    list_display = ('itn', 'last_name', 'first_name', 'middle_name')
    fields = ['itn', ('last_name', 'first_name', 'middle_name')]

# Register the Admin classes for Deals using the decorator

@admin.register(Deals)
class DealsAdmin(admin.ModelAdmin):
    list_display = ('itn', 'sum', 'tax', 'date', 'employee', 'approve')
    list_filter = ('employee', 'date', 'approve')
    fields = [('itn', 'date', 'employee', 'approve'), ('sum', 'tax'), 'comments']