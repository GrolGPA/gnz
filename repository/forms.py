from django import forms
from .models import Deals
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import datetime
from repository.models import Employees


class DealForm(forms.ModelForm):
    class Meta:
        model = Deals
        # exclude = ('itn',)
        fields = ['itn','sum','tax','date','employee','comments']
        # widgets = {"itn": forms.HiddenInput()}

        def __init__(self, *args, **kwargs):
            self.itn = kwargs.pop('clients')
            super(DealForm, self).__init__(*args, **kwargs)

        def clean_title(self):
            itn = self.cleaned_data['itn']
            if Deals.objects.filter(itn=self.itn).exists():
                raise forms.ValidationError("You have already written a book with same title.")
            return itn




class ApproveDealForm(forms.Form):
    approve = forms.BooleanField(help_text=_("tr_Approve"))



# class AddDealForm(forms.Form):
#
#     itn = forms.CharField(required=True, help_text=_('rt_enter ITN or series and passport number'))
#     sum = forms.IntegerField(required=True, help_text=_('tr_enter sum of deal'))
#     tax = forms.IntegerField(help_text=_('tr_Enter sum of the tax'))
#     date = forms.DateField(initial=datetime.date.today(), localize=True, help_text=_("tr_Enter the date of deal"))
#     employee = forms.ModelChoiceField(queryset=Employees.objects.all(), help_text=_('tr_choise_employee'))
#     comments = forms.T(max_length=1000,  help_text=_("tr_Enter your comments"))
#
#
#     def clean_deal_date(self):
#         date = self.deal_date['date']
#
#         # Check that the deal date is not in the future
#         if date > datetime.date.today():
#             raise ValidationError(_('tr_Invalid date - deal in future'))
#
#         # Check that the date does not go beyond the "lower" limit (-2 months)
#         if date > datetime.date.today() - datetime.timedelta(mounth=2):
#             raise ValidationError(_('tr_Invalid date - deal more than 2 month ago'))
#
#         return date