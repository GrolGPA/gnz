# Generated by Django 2.1.8 on 2019-04-04 06:59

import datetime
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Clients',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('itn', models.IntegerField(help_text='Enter IPN', max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Deals',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text='Unique ID for a deal', primary_key=True, serialize=False, unique=True)),
                ('sum', models.IntegerField(help_text='Enter sum of the deal', validators=[django.core.validators.MinValueValidator(1000), django.core.validators.MaxValueValidator(10000000)])),
                ('tax', models.IntegerField(help_text='Enter sum of the tax', validators=[django.core.validators.MinValueValidator(10), django.core.validators.MaxValueValidator(100000)])),
                ('date', models.DateField(default=datetime.date.today, help_text='Enter the date of deal')),
                ('comments', models.TextField(blank=True, help_text='Enter your comments', max_length=1000, null=True)),
                ('approve', models.BooleanField(default=False, help_text='Approve')),
            ],
        ),
        migrations.CreateModel(
            name='Employees',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(help_text='Enter manager name', max_length=30)),
                ('last_name', models.CharField(help_text='Enter manager name', max_length=30)),
            ],
        ),
        migrations.AddField(
            model_name='deals',
            name='employee',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='repository.Employees'),
        ),
        migrations.AddField(
            model_name='deals',
            name='itn',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='repository.Clients'),
        ),
    ]
