from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse #Used to generate URLs by reversing the URL patterns
from datetime import date
import uuid
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.translation import ugettext_lazy as _


class Employees(models.Model):

    first_name = models.CharField(max_length=30, help_text=_("tr_Enter manager name"), verbose_name=_('tr_first name'))
    last_name = models.CharField(max_length=30, help_text=_("tr_Enter manager name"), verbose_name=_('tr_last name'))

    def __str__(self):
        """
        String for representing the Model object (in Admin site etc.)
        """
        return '{1} {0}'.format(self.first_name, self.last_name)


class Clients(models.Model):

    itn = models.CharField(max_length=10, help_text=_("tr_Enter IPN"), verbose_name=_('tr_itn'))
    first_name = models.CharField(max_length=20, null=True, blank=True, help_text=_("tr_Enter first name"), verbose_name=_('tr_first name'))
    last_name = models.CharField(max_length=20, null=True, blank=True, help_text=_("tr_Enter last name"), verbose_name=_('tr_last name'))
    middle_name = models.CharField(max_length=20, null=True, blank=True, help_text=_("tr_Enter middle name"), verbose_name=_('tr_middle name'))

    def __str__(self):

        return self.itn


class Deals(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True, help_text=_("tr_Unique ID for a deal"))
    itn = models.ForeignKey(Clients, on_delete=models.SET_NULL, null=True, verbose_name=_('tr_itn'))
    sum = models.IntegerField(validators=[MinValueValidator(1000), MaxValueValidator(10000000)], help_text=_("tr_Enter sum of the deal"), verbose_name=_('tr_sum'))
    tax = models.IntegerField(validators=[MinValueValidator(10), MaxValueValidator(100000)], help_text=_("tr_Enter sum of the tax"), verbose_name=_('tr_sum of the tax'))
    date = models.DateField(default=date.today, help_text=_("tr_Enter the date of deal"), verbose_name=_('tr_date of the deal'))
    employee = models.ForeignKey(Employees, on_delete=models.SET_NULL, null=True, verbose_name=_('tr_employee'))
    comments = models.TextField(max_length=1000, null=True, blank=True, help_text=_("tr_Enter your comments"), verbose_name=_('tr_comments'))
    approve = models.BooleanField(default=False, help_text=_("tr_Approve"), verbose_name=_('tr_approve'))

    def __str__(self):

        return '{0}, {1}, {2}, {3}, {4}, {5}, {6}'.format(self.itn, self.sum, self.tax, self.date, self.employee, self.comments, self.approve)

    def get_absolute_url(self):
        """
        Returns the url to access a particular deal instance.
        """
        return reverse('deal-detail', args=[str(self.id)])

