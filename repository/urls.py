from django.urls import path, re_path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    re_path(r'^deals/$', views.DealsListView.as_view(), name='deals'),
    re_path(r'^deals/(?P<pk>[-\d\w]+)$', views.DealDetailView.as_view(), name='deal-detail'),
    # re_path(r'^deals/add/$', views.add_deal, name='add-deal'),
]

urlpatterns += [
    re_path(r'^deals/create/$', views.DealCreate.as_view(), name='deal_create'),
    re_path(r'^deals/(?P<pk>[-\d\w]+)/update/$', views.DealUpdate.as_view(), name='deal_update'),
    re_path(r'^deals/(?P<pk>[-\d\w]+)/delete/$', views.DealDelete.as_view(), name='deal_delete'),
]

# urlpatterns += [
#     re_path(r'^deals/(?P<pk>[-\d\w]+)/approve/$', views.approve_deal, name='approve-deal'),
# ]
