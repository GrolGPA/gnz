from django.shortcuts import render
from .models import Employees, Clients, Deals
from django.views import generic
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import permission_required
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from datetime import date
from .forms import ApproveDealForm
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


@login_required
def index(request):
    """
    View function for the home page
    """
    # Counters
    num_deals = Deals.objects.all().count()
    num_clients = Clients.objects.all().count()
    # Unapproved deals ( status = 'False')
    num_unapproved_deals = Deals.objects.filter(approve='False').count()
    num_employees = Employees.objects.count()  # Method 'all()' applied by default.

    # Rendering HTML-template index.html with data inside
    return render(
        request,
        'index.html',
        context={'num_deals': num_deals, 'num_clients': num_clients,
                 'num_unapproved_deals': num_unapproved_deals, 'num_employees': num_employees},
    )


class DealsListView(LoginRequiredMixin, generic.ListView):
    model = Deals
    template_name = 'deals_list.html'
    paginate_by = 25



class DealDetailView(LoginRequiredMixin, generic.DetailView):
    model = Deals
    template_name = 'deal_detail.html'

"""
Classes for creation, updation and deletion of deals
"""

from .forms import DealForm
class DealCreate(CreateView):

    model = Deals
    form_class = DealForm
    # fields = ['itn','sum','tax','date','employee','comments']
    success_url = reverse_lazy('deals')

    def form_valid(self, form):
        form.instance.itn = self.request.clients
        return super(DealCreate.self).form_valid(form)

    #
    # def get_form(self, form_class):
    #     initials = {
    #         "client": self.request.clients
    #     }
    #     form = form_class(initial=initials)
    #     return form


    # def form_valid(self, form):
    #     form.instance.itn = self.request.Clients
    #     return super(DealCreate, self).form_valid(form)



class DealUpdate(UpdateView):
    model = Deals
    fields = '__all__'

class DealDelete(DeleteView):
    model = Deals
    success_url = reverse_lazy('deals')

"""
Approving deal
"""
# @permission_required('repository.can_approve')
# def approve_deal(request, pk):
#     """
#         View function for approving a specific deal by approver
#         """
#     deal = get_object_or_404(Deals, pk=pk)
#
#     # If this is a POST request then process the Form data
#     if request.method == 'POST':
#
#         # Create a form instance and populate it with data from the request (binding):
#         form = ApproveDealForm(request.POST)
#
#         # Check if the form is valid:
#         if form.is_valid():
#             # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
#
#             deal.save()
#
#             # redirect to a new URL:
#             return HttpResponseRedirect(reverse('deals'))
#
#     # If this is a GET (or any other method) create the default form.
#     else:
#         form = ApproveDealForm(initial=False)
#
#     return render(request, 'repository/approve_deal.html', {'form': form, 'Deal': deal})